package com.arturorsmd.asynctask;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnDownloadSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progress_bar);
        btnDownloadSong = findViewById(R.id.btn_download_song);

        btnDownloadSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SegundoPlano().execute();
            }
        });


    }

    private class SegundoPlano extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, "Preparing download", Toast.LENGTH_SHORT).show();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            for (int i = 0; i < 5; i++){
                downloadSong();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(MainActivity.this, "Finishing download", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

        }


    }


    public void downloadSong(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void downloadSongMainThread(){

        Toast.makeText(MainActivity.this, "Preparing download", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < 5; i++){
            progressBar.setVisibility(View.VISIBLE);
            downloadSong();
        }

        Toast.makeText(MainActivity.this, "Finishing download", Toast.LENGTH_SHORT).show();


    }
}
